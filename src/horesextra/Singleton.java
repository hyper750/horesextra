/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package horesextra;

/**
 *
 * @author RaulM
 */
class Singleton {
    private static final Singleton INSTANCE = new Singleton();
    private Guardar guardar = new Guardar();
    private Carregar carregar = new Carregar();
    private Singleton(){
    }
    
    public static Singleton getInstance(){
        return INSTANCE;
    }
    
    public Guardar getGuardar(){
        return guardar;
    }
    
    public Carregar getCarregar(){
        return carregar;
    }
}
