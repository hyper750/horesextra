/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package horesextra;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.Objects;

/**
 *
 * @author RaulM
 */
class Mes implements Serializable, Comparable<Mes>{
    private final ArrayList<Dia> dies;
    private final GregorianCalendar fecha;
    
    public Mes(final int any, final int mes){
        this.dies = new ArrayList<Dia>();
        this.fecha = new GregorianCalendar(any, mes, 1);
    }
    
    public Dia getDia(final Dia arg){
        return dies.get(dies.indexOf(arg));
    }
    
    public int getMes(){
        return fecha.get(Calendar.MONTH);
    }
    
    public int getAny(){
        return fecha.get(Calendar.YEAR);
    }
    
    public Iterator<Dia> iterator(){
        return dies.iterator();
    }
    
    public double getExtraMes(final int hcontracte){
        double resultat = 0;
        Dia d;
        for(Iterator<Dia> it = dies.iterator(); it.hasNext();){
            d = it.next();
            resultat += d.getExtra(hcontracte);
        }
        return resultat;
    }
    
    public void afegirDia(final int dia, final double inici, final double fin){
        Dia d = new Dia(dia, inici, fin);
        
        //Si es dia ja existeix mofidicar
        if(dies.contains(d)){
            dies.get(dies.indexOf(d)).modificar(inici, fin);
        }
        else{
            dies.add(d);
        }
        
    }
    
    public void afegirDia(final int dia, final double inici, final double fin, final double inicicap, final double fincap){
        Dia d = new Dia(dia, inici, fin, inicicap, fincap);
        
        //Si dies conte aquest dia modificar-lo si no crear-lo
        if(dies.contains(d)){
            dies.get(dies.indexOf(d)).modificar(inici, fin, inicicap, fincap);
        }
        else{
            dies.add(d);
        }
    }
    
    public void llevarDia(final int dia){
        dies.remove(new Dia(dia, 0, 0));//S'equals de Dia nomes mira es dia no s'hora
    }
    
    public GregorianCalendar getFecha() {
        return (GregorianCalendar)fecha.clone();
    }
    
    public boolean contains(final Dia arg){
        return dies.contains(arg);
    }
    
    @Override
    public int compareTo(final Mes arg){
        return fecha.compareTo(arg.getFecha());
    }
    
    public void sort(){
        Collections.sort(dies);
    }
    
    @Override
    public boolean equals(final Object arg){
        if(this == arg){
            return true;
        }
        if(arg == null){
            return false;
        }
        if(arg instanceof Mes){
            Mes obj = (Mes)arg;
            return getMes() == obj.getMes() && getAny() == obj.getAny();
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 43 * hash + Objects.hashCode(this.fecha);
        return hash;
    }
    
    @Override
    public String toString(){
        return "Any: " + getAny() + " Mes: " + getMes() + dies;
    }
}
