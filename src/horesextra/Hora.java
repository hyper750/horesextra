/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package horesextra;

import java.io.Serializable;

/**
 *
 * @author RaulM
 */
class Hora implements Serializable{
    private double hora;
    public Hora(final double arg){
        this.hora = arg;
    }
    
    public Hora(final Hora arg){
        this(arg.getHora());
    }
    
    public double getHora(){
        return hora;
    }
    
    public String getHoraString(){
        int hora = (int)getHora();
        int minuts = (int)((double)((double)getHora() - hora)*60);
        if(minuts < 0){
            minuts *= -1;
        }
        String sminuts = String.valueOf(minuts);
        while(sminuts.length() < 2){
            sminuts += "0";
        }
        String resultat = String.valueOf(hora) + ":" + sminuts;
        return resultat;
    }
    
    @Override
    public boolean equals(final Object arg){
        if(this == arg){
            return true;
        }
        if(arg == null){
            return false;
        }
        if(arg instanceof Hora){
            Hora h = (Hora)arg;
            return getHora() == h.getHora();
        }
        return false;
    }
    
    public void modificar(final double temps){
        this.hora = temps;
    }
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + (int) (Double.doubleToLongBits(this.hora) ^ (Double.doubleToLongBits(this.hora) >>> 32));
        return hash;
    }
    
    @Override
    public String toString(){
        return getHoraString();
    }
}
