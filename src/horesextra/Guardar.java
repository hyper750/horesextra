/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package horesextra;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

/**
 *
 * @author RaulM
 */
class Guardar {
    public Guardar(){
        
    }
    
    public boolean guardar(final Mesos arg, final String arxiu){
        try{
            ObjectOutputStream oos = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(arxiu)));
            oos.writeObject(arg);
            oos.close();
        }
        catch(IOException e){
            e.printStackTrace();
            return false;
        }
        
        
        return true;
    }
}
