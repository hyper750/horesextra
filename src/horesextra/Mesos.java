/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package horesextra;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Objects;

/**
 *
 * @author RaulM
 */
public class Mesos implements Serializable{
    private ArrayList<Mes> mesos;
    
    public Mesos(){
        mesos = new ArrayList<Mes>();
    }
    
    public void afegirMes(final int any, final int mes){
        mesos.add(new Mes(any, mes));
    }
    
    public boolean contains(final Mes arg){
        return mesos.contains(arg);
    }
    
    public ArrayList<Mes> getMesos(){
        return new ArrayList<Mes>(mesos);
    }
    
    public Mes getMes(final int indx){
        return mesos.get(indx);
    }
    
    public Mes getMes(final Mes indx){
        return (mesos.indexOf(indx) >= 0)? mesos.get(mesos.indexOf(indx)) : null;
    }
    
    public void llevarMes(final int any, final int mes){
        mesos.remove(new Mes(any, mes));
    }
    
    public void sort(){
        Collections.sort(mesos);
    }
    
    @Override
    public boolean equals(final Object arg){
        if(this == arg){
            return true;
        }
        if(arg == null){
            return false;
        }
        if(arg instanceof Mesos){
            Mesos m = (Mesos)arg;
            return mesos.equals(m.getMesos());
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + Objects.hashCode(this.mesos);
        return hash;
    }
    
    @Override
    public String toString(){
        return "Mesos: " + getMesos();
    }
}
