/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package horesextra;

import java.awt.Color;
import java.awt.FileDialog;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.WindowConstants;

/**
 *
 * @author RaulM
 */
class HoresGrafiques {
    private final JFrame main = new JFrame("Hores extra");
    private final JPanel pan = (JPanel)main.getContentPane();
    
    //MES
    private final JComboBox llistaMes;
    private final JComboBox llistaMesm;
    private final JComboBox llistaDia;
    private final JComboBox llistaContracte;
    private final DefaultComboBoxModel<Integer> modelDia;
    
    //Dia Hores
    private final JComboBox comboHoresDia;
    private final JComboBox comboHoresDiaFin;
    private final JComboBox comboHoresCap;
    private final JComboBox comboHoresCapFin;
    
    
    //Resultat
    private final JTextArea areaResultat;
    
    Mesos obj = new Mesos();
    
    public HoresGrafiques(){
        main.setSize(1250, 800);
        main.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        pan.setLayout(new GridBagLayout());
        pan.setBackground(new Color(220, 220, 220));
        
        //BARRA MENU
        JMenuBar menu = new JMenuBar();
        JMenu arxiu = new JMenu("Arxiu");
        JMenuItem nouMenu = new JMenuItem("Nou");
        JMenuItem guardarMenu = new JMenuItem("Guardar");
        JMenuItem carregarMenu = new JMenuItem("Carregar");
        
        arxiu.add(nouMenu);
        arxiu.add(guardarMenu);
        arxiu.add(carregarMenu);
        
        menu.add(arxiu);
        
        main.setJMenuBar(menu);
        
        //PAN TITOL
        JPanel panTitol = new JPanel(new FlowLayout(FlowLayout.CENTER));
        panTitol.setBackground(new Color(180, 180, 180));
        JLabel titol = new JLabel("Hores Extra");
        titol.setFont(new Font("Comic Sans MS", Font.PLAIN, 24));
        panTitol.add(titol);
        
        
        //Sobre Mes
        JPanel panMes = new JPanel(new GridBagLayout());
        panMes.setBackground(new Color(180, 180, 180));
        DefaultComboBoxModel<Integer> modelMes = new DefaultComboBoxModel<Integer>();
        llistaMes = new JComboBox(modelMes);
        DefaultComboBoxModel<Integer> modelMesm = new DefaultComboBoxModel<Integer>();
        modelDia = new DefaultComboBoxModel<Integer>();
        llistaDia = new JComboBox(modelDia);
        llistaMesm = new JComboBox(modelMesm);
        DefaultComboBoxModel<Integer> modelContracte = new DefaultComboBoxModel<Integer>();
        llistaContracte = new JComboBox(modelContracte);
        JLabel lContracte = new JLabel("Hores contracte");
        //Generar hores des contracte
        for(int x = 6; x <= 8; x += 2){
            modelContracte.addElement(x);
        }
        //Generar anys
        for(int x = 2010; x <= 2100; x++){
            modelMes.addElement(x);
        }
        //Generar mesos
        for(int x = 1; x <= 12; x++){
            modelMesm.addElement(x);
        }
        //Generar Dies
        for(int x = 1; x <= 31; x++){
            modelDia.addElement(x);
        }
        
        JLabel lany = new JLabel("Any ");
        JLabel lmes = new JLabel("Mes ");
        JLabel ldia = new JLabel("Dia ");
        
        JButton borrarMes = new JButton("Borrar nes");
        
        
       
        //Sobre dia
        JPanel panDia = new JPanel(new GridBagLayout());
        panDia.setBackground(new Color(180, 180, 180));
        
        //PAN TITOL DIA
        JPanel panDiadia = new JPanel(new GridBagLayout());
        panDiadia.setBackground(new Color(210, 210, 210));
        JLabel lDiadia = new JLabel("Mati");
        
        //BODY DIA
        JPanel panBodyDia = new JPanel(new GridBagLayout());
        panBodyDia.setBackground(new Color(210, 210, 210));
        JLabel lIniciDia = new JLabel("Hora inici");
        DefaultComboBoxModel<Hora> modelHoresDia = new DefaultComboBoxModel<Hora>();
        DefaultComboBoxModel<Hora> modelHoresDiaFin = new DefaultComboBoxModel<Hora>();
        //MODEL CAPVESPRE
        DefaultComboBoxModel<Hora> modelHoresCap = new DefaultComboBoxModel<Hora>();
        DefaultComboBoxModel<Hora> modelHoresCapFin = new DefaultComboBoxModel<Hora>();
        comboHoresDia = new JComboBox(modelHoresDia);
        
        //Model Hores
        for(double x = 0; x <= 23.75; x += 0.25){
            modelHoresDia.addElement(new Hora(x));
            modelHoresDiaFin.addElement(new Hora(x));
            //CAPVESPRE
            modelHoresCap.addElement(new Hora(x));
            modelHoresCapFin.addElement(new Hora(x));
        }
        
        JPanel panBodyDiad = new JPanel(new GridBagLayout());
        panBodyDiad.setBackground(new Color(210, 210, 210));
        JLabel lFinDia = new JLabel("Hora fin");
        comboHoresDiaFin = new JComboBox(modelHoresDiaFin);
        
        
        
        
        JPanel panDiacap = new JPanel(new GridBagLayout());
        panDiacap.setBackground(new Color(210, 210, 210));
        panDiacap.add(new JLabel("CAPVESPRE"));
        
        JPanel panDiaCapIni = new JPanel(new GridBagLayout());
        panDiaCapIni.setBackground(new Color(210, 210, 210));
        JLabel lIniciCap = new JLabel("Hora inici");
        comboHoresCap = new JComboBox(modelHoresCap);
        
        
        JPanel panDiaCapFin = new JPanel(new GridBagLayout());
        panDiaCapFin.setBackground(new Color(210, 210, 210));
        JLabel lFinCap = new JLabel("Hora fin");
        comboHoresCapFin = new JComboBox(modelHoresCapFin);
        
        //Buttons dia
        JButton borrarDia = new JButton("Borrar dia");
        JButton butoDia = new JButton("Nou dia");
        
        //Sobre resultat
        JPanel panRes = new JPanel(new GridBagLayout());
        panRes.setBackground(new Color(180, 180, 180));
        areaResultat = new JTextArea();
        areaResultat.setBackground(new Color(210, 210, 210));
        areaResultat.setFont(new Font("Comic Sans MS", Font.BOLD, 24));
        areaResultat.setEditable(false);
        JScrollPane resultatScroll = new JScrollPane(areaResultat);
        
        //MES
        panMes.add(lany, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(10, 10, 10, 10), 0, 0));
        panMes.add(llistaMes, new GridBagConstraints(1, 0, 1, 1, 0.1, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(10, 10, 10, 10), 0, 0));
        panMes.add(lmes, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(10, 10, 10, 10), 0, 0));
        panMes.add(llistaMesm, new GridBagConstraints(3, 0, 1, 1, 0.1, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(10, 10, 10, 10), 0, 0));
        panMes.add(ldia, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(10, 10, 10, 10), 0, 0));
        panMes.add(llistaDia, new GridBagConstraints(5, 0, 1, 1, 0.1, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(10, 10, 10, 10), 0, 0));
        panMes.add(lContracte, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(10, 10, 10, 10), 0, 0));
        panMes.add(llistaContracte, new GridBagConstraints(7, 0, 1, 1, 0.1, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(10, 10, 10, 10), 0, 0));
        panMes.add(borrarMes, new GridBagConstraints(0, 1, 8, 1, 0.1, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(10, 210, 10, 200), 0, 0));
        
        //DIA I HORES
            //DIA titol
            panDiadia.add(lDiadia, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(10, 10, 10, 10), 0, 0));//Pan titol hores dia
            
            //Body dia
            panBodyDia.add(lIniciDia, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(10, 10, 10, 10), 0, 0));
            panBodyDia.add(comboHoresDia, new GridBagConstraints(1, 0, 1, 1, 0.1, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(10, 10, 10, 10), 0, 0));
            panBodyDiad.add(lFinDia, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(10, 10, 10, 10), 0, 0));
            panBodyDiad.add(comboHoresDiaFin, new GridBagConstraints(1, 0, 1, 1, 0.1, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(10, 19, 10, 10), 0, 0));
            
            //CAPVESPRE
            panDiaCapIni.add(lIniciCap, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(10, 10, 10, 10), 0, 0));
            panDiaCapIni.add(comboHoresCap, new GridBagConstraints(1, 0, 1, 1, 0.1, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(10, 10, 10, 10), 0, 0));
            
            panDiaCapFin.add(lFinCap, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(10, 10, 10, 10), 0, 0));
            panDiaCapFin.add(comboHoresCapFin, new GridBagConstraints(1, 0, 1, 1, 0.1, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(10, 19, 10, 10), 0, 0));
                    
            //TITOLS
            panDia.add(panDiadia, new GridBagConstraints(0, 0, 1, 1, 0.1, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(10, 10, 10, 10), 0, 0));
            panDia.add(panDiacap, new GridBagConstraints(1, 0, 1, 1, 0.1, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(10, 10, 10, 10), 0, 0));
            
            //COS
            panDia.add(panBodyDia, new GridBagConstraints(0, 1, 1, 1, 0.1, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(10, 10, 10, 10), 0, 0));
            panDia.add(panBodyDiad, new GridBagConstraints(0, 2, 1, 1, 0.1, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(10, 10, 10, 10), 0, 0));
            
            //COS CAPVESPRE
            panDia.add(panDiaCapIni, new GridBagConstraints(1, 1, 1, 1, 0.1, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(10, 10, 10, 10), 0, 0));
            panDia.add(panDiaCapFin, new GridBagConstraints(1, 2, 1, 1, 0.1, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(10, 10, 10, 10), 0, 0));
            
            //BOTO
            panDia.add(borrarDia, new GridBagConstraints(0, 3, 1, 1, 0.1, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(10, 10, 10, 10), 0, 0));
            panDia.add(butoDia, new GridBagConstraints(1, 3, 1, 1, 0.1, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(10, 10, 10, 10), 0, 0));
            
            //RESULTAT
            panRes.add(resultatScroll, new GridBagConstraints(0, 0, 1, 1, 0.1, 0.1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(10, 10, 10, 10), 0, 0));
        //PAN TOT
        pan.add(panTitol, new GridBagConstraints(0, 0, 2, 1, 0.1, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(20, 20, 20, 20), 5, 5));
        pan.add(panMes, new GridBagConstraints(0, 1, 1, 1, 0.1, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(20, 20, 20, 20), 5, 5));
        pan.add(panDia, new GridBagConstraints(1, 1, 1, 1, 0.1, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(20, 20, 20, 20), 5, 5));
        pan.add(panRes, new GridBagConstraints(0, 2, 2, 1, 0.1, 0.1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(20, 20, 20, 20), 5, 5));
        
        
        //LISTENERS
        
        llistaMes.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                actualitzarMesAny();
            }
        });
        
        llistaMesm.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                actualitzarMesAny();
            }
        });
        
        llistaDia.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                seleccioHora();
            }
        });
        
        butoDia.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                    int any = (Integer)llistaMes.getSelectedItem();
                    int mes = (Integer)llistaMesm.getSelectedItem();  
                    int dia = (Integer)llistaDia.getSelectedItem();
                    double horaIni = ((Hora)comboHoresDia.getSelectedItem()).getHora();
                    double horaFin = ((Hora)comboHoresDiaFin.getSelectedItem()).getHora();
                    double horaIniCap = ((Hora)comboHoresCap.getSelectedItem()).getHora();
                    double horaFinCap = ((Hora)comboHoresCapFin.getSelectedItem()).getHora();
                    if(!obj.contains(new Mes(any, mes))){
                        obj.afegirMes(any, mes);
                    }
                    obj.getMes(new Mes(any, mes)).afegirDia(dia, horaIni, horaFin, horaIniCap, horaFinCap);
                    obj.getMes(new Mes(any, mes)).sort();
                    mostrarResultat();
            }
        });
        
        borrarMes.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                int any = (Integer)llistaMes.getSelectedItem();
                int mes = (Integer)llistaMesm.getSelectedItem();  
                obj.llevarMes(any, mes);
                seleccioHora();
            }
        });
        
        borrarDia.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                int any = (Integer)llistaMes.getSelectedItem();
                int mes = (Integer)llistaMesm.getSelectedItem();  
                int dia = (Integer)llistaDia.getSelectedItem();
                
                if(obj.contains(new Mes(any, mes)) && obj.getMes(new Mes(any, mes)).contains(new Dia(dia, 0, 0))){//Si existeix es dia i es mes
                    obj.getMes(new Mes(any, mes)).llevarDia(dia);
                    seleccioHora();
                }
                mostrarResultat();
            }
        });
        
        llistaContracte.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                mostrarResultat();
            }
        });
        
        //LISTENERS MENU
        nouMenu.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                obj = new Mesos();
                llistaDia.setSelectedIndex(0);
                llistaMes.setSelectedIndex(0);
                llistaMesm.setSelectedIndex(0);
                comboHoresDia.setSelectedIndex(0);
                comboHoresDiaFin.setSelectedIndex(0);
                comboHoresCap.setSelectedIndex(0);
                comboHoresCapFin.setSelectedIndex(0);
            }
        });
        
        carregarMenu.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                FileDialog fd = new FileDialog(main, "Carregar", FileDialog.LOAD);
                fd.setDirectory(".");
                fd.setFile("*.hg");
                fd.setVisible(true);
                String arxiu = fd.getFile();
                if(arxiu != null){
                    obj = Singleton.getInstance().getCarregar().carregar(arxiu);
                    seleccioHora();
                }
            }
        });
        
        guardarMenu.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                FileDialog fd = new FileDialog(main, "Guardar", FileDialog.SAVE);
                fd.setDirectory(".");
                fd.setFile("*.hg");
                fd.setVisible(true);
                String arxiu = fd.getFile();
                if(arxiu != null){
                    if(!arxiu.contains(".hg")){
                        arxiu += ".hg";
                    }
                    Singleton.getInstance().getGuardar().guardar(obj, arxiu);
                }
            }
        });
        mostrarResultat();
        main.setLocationRelativeTo(null);
        main.setVisible(true);
    }
    
    private boolean esBisiesto(final int any){
        if(any % 4 == 0){
            if(any % 100 == 0){
                if(any % 400 == 0){
                    return true;
                }
                return false;
            }
            return true;
        }
        return false;
    }
    
    private void actualitzarMesAny(){
        int any = (Integer)llistaMes.getSelectedItem();
        int mes = (Integer)llistaMesm.getSelectedItem();  
        int dies = -1;
        //Cada vegada que selecciones un nou mes o any canviar es modelDia       
        llistaDia.removeAllItems();
        switch(mes){
            case 1: dies = 31; break;
            case 2: if(esBisiesto(any)){
                dies = 29;
            }
            else{
                dies = 28;
            }
            break;
            case 3: dies = 31; break;
            case 4: dies = 30; break;
            case 5: dies = 31; break;
            case 6: dies = 30; break;
            case 7: dies = 31; break;
            case 8: dies = 31; break;
            case 9: dies = 30; break;
            case 10: dies = 31; break;
            case 11: dies = 30; break;
            case 12: dies = 31; break;
            default: dies = 0;
        }
        for(int x = 1; x <= dies; x++){
            modelDia.addElement(x);
        }
        //Crear Mes de s'any que toca
        if(!obj.contains(new Mes(any, mes))){
            obj.afegirMes(any, mes);
        }
        mostrarResultat();
    }
    
    private void seleccioHora(){
        int any = (Integer)llistaMes.getSelectedItem();
        int mes = (Integer)llistaMesm.getSelectedItem();  
        int dia = -1;
        Dia diaActual = null;
        if((Integer)llistaDia.getSelectedItem() != null){
            dia = (Integer)llistaDia.getSelectedItem();
        }
        
        //Si es mes i es dia existeixen posar ses hores seleccionades que toca
        if(dia != -1 && obj.contains(new Mes(any, mes)) && obj.getMes(new Mes(any, mes)).contains(new Dia(dia, 0, 0))){
            diaActual = obj.getMes(new Mes(any, mes)).getDia(new Dia(dia, 0, 0));
            comboHoresDia.setSelectedItem(new Hora(diaActual.getIniciDia()));
            comboHoresDiaFin.setSelectedItem(new Hora(diaActual.getFinDia()));
            comboHoresCap.setSelectedItem(new Hora(diaActual.getIniciCap()));
            comboHoresCapFin.setSelectedItem(new Hora(diaActual.getFinCap()));
        }
        else{
            comboHoresDia.setSelectedIndex(0);
            comboHoresDiaFin.setSelectedIndex(0);
            comboHoresCap.setSelectedIndex(0);
            comboHoresCapFin.setSelectedIndex(0);
        }
        mostrarResultat();
    }
    
    private void mostrarResultat(){
        //Mostrar resultat a nes jtextarea
        int any = (Integer)llistaMes.getSelectedItem();
        int mes = (Integer)llistaMesm.getSelectedItem();  
        Dia d1;
        double extraTotal = 0;
        String res = "DIA   INICI MATI  FIN MATI    INICI CAPVESPRES    FIN CAPVESPRE   HORA    EXTRA\n";
        if(obj.contains(new Mes(any, mes))){
            for(Iterator<Dia> it = obj.getMes(new Mes(any, mes)).iterator(); it.hasNext();){
                d1 = it.next();     
                res += d1.getDia() + "\t" + d1.getIniciDia().getHoraString() + "\t" + d1.getFinDia() + "\t      " + d1.getIniciCap() + "\t\t " + d1.getFinCap() + "\t " + d1.getHoraString() + "      " + d1.getExtraString((int)llistaContracte.getSelectedItem()) + "\n";
                extraTotal += d1.getExtra((int)llistaContracte.getSelectedItem());
            }
            res += "\n\nHores extra totals: " + new Hora(extraTotal).getHoraString();
        }
        
        areaResultat.setText(res);
    }
}
