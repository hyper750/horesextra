/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package horesextra;

import java.io.Serializable;

/**
 *
 * @author RaulM
 */
class Dia implements Comparable<Dia>, Serializable{
    private final byte dia;
    private final Hora inici, fin, iniciCap, finCap;
    
    public Dia(final int dia, final double inici, final double fin){
        this.dia = (byte)dia;
        this.inici = new Hora(inici);
        this.fin = new Hora(fin);
        this.iniciCap = new Hora(0);
        this.finCap = new Hora(0);
    }
    
    public Dia(final int dia, final double inici, final double fin, final double inicicap, final double fincap){
        this.dia = (byte)dia;
        this.inici = new Hora(inici);
        this.fin = new Hora(fin);
        this.iniciCap = new Hora(inicicap);
        this.finCap = new Hora(fincap);
    }
    
    public Hora getIniciDia(){
        return new Hora(inici);
    }
    
    public Hora getFinDia(){
        return new Hora(fin);
    }
    
    public Hora getIniciCap(){
        return new Hora(iniciCap);
    }
    
    public Hora getFinCap(){
        return new Hora(finCap);
    }
    
    public int getDia(){
        return dia;
    }
    
    public double getHora(){
        return (fin.getHora()-inici.getHora()) + (finCap.getHora()-iniciCap.getHora());
    }
    
    public String getHoraString(){
        return new Hora(getHora()).getHoraString();
    }
    
    public double getExtra(final int contracte){
        return getHora()-contracte;
    }
    
    public String getExtraString(final int contracte){
        return new Hora(getExtra(contracte)).getHoraString();
    }
    
    public void modificar(final double inici, final double fin){
        this.inici.modificar(inici);
        this.fin.modificar(fin);
    }
    
    public void modificar(final double inici, final double fin, final double inicicap, final double fincap){
        modificar(inici, fin);
        this.iniciCap.modificar(inicicap);
        this.finCap.modificar(fincap);
    }
    
    @Override
    public int compareTo(final Dia arg){
        return getDia() - arg.getDia();
    }
    
    @Override
    public boolean equals(final Object arg){
        if(this == arg){
            return true;
        }
        if(arg == null){
            return false;
        }    
        if(arg instanceof Dia){
            Dia obj = (Dia)arg;
            return this.getDia() == obj.getDia();
        }
        return false;
    }

   
    
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 83 * hash + this.dia;
        return hash;
    }
    
    @Override
    public String toString(){
        return "FECHA: " + getDia() + " -> " + getHora() + "h";
    }
}
